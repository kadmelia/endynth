const { app, BrowserWindow } = require('electron');
const Tone  = require('tone');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline')

function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })

  win.loadFile('index.html')
  win.webContents.openDevTools()
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
});

app.once('ready', () => {
  const port = new SerialPort('/dev/ttyACM0', {
    baudRate: 38400
  });
  const parser = new Readline();
  port.pipe(parser);
  parser.on('data', function (data) {
    let eventNames = {
      "P": 'note-played',
      "R": 'note-released',
    };
    // Send event of played note
    BrowserWindow.getAllWindows()[0].webContents.send(
      eventNames[data.charAt(0)], parseInt(data.substring(1), 10));
  });
})

