const { ipcRenderer } = require('electron');
// const usb = require("webusb").usb;

class Keyboard {
    constructor(synth, effect) {
        this.synth = synth;
        // synth.connect(effect.getEffect());
        this.handleKeypresses();
        this.playing = [];
    }

    handleKeypresses() {
        document.addEventListener('keydown', this.onKeyPress.bind(this));
        document.addEventListener('keyup', this.onKeyUp.bind(this));
        ipcRenderer.on('note-played', this.onPianoKeyPress.bind(this));
        ipcRenderer.on('note-released', this.onPianoKeyRelease.bind(this));
    }
    
    onKeyPress(event) {
        if (!event.key.match(/^[azertyu]$/)){
            return;
        }
        const note = this.getNoteFromKey(event.key);

        this.playNote(note);
       
    }

    onKeyUp(event) {
        const note = this.getNoteFromKey(event.key);
        this.releaseNote();
    }

    onPianoKeyPress(event, note) {
        console.log(this.convertMidiToNote(note));
        this.playNote(this.convertMidiToNote(note));
    }

    onPianoKeyRelease(event, note) {
        // console.log(this.convertMidiToNote(note));
        this.releaseNote(this.convertMidiToNote(note));
    }

    playNote(note) {
        if (note === null || this.playing.includes(note)) {
            return;
        }
        this.playing.push(note);
        this.synth.triggerAttack(note);
    }

    releaseNote(note) {
        this.playing.splice(this.playing.indexOf(note), 1);
        this.synth.triggerRelease(note);
    }

    getNoteFromKey(key) {

        const notes = {
            'a': 'C',
            'z': 'D',
            'e': 'E',
            'r': 'F',
            't': 'G',
            'y': 'A',
            'u': 'B'
        };

        return notes[key] ?  notes[key] + "4" : null;
    }

    convertMidiToNote(midi) {
        const notes = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'];
        const note = notes[midi % 12];
        const octave = Math.floor(midi / 12) + 2;
        return note + octave;
    }

}
module.exports = Keyboard;