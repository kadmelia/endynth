const Tone = require('tone');

class Reverb {
    
    constructor() {
        this.effect = null;
        this.initEffect();
    }

    initEffect() {
        this.effect = new Tone.Reverb().toDestination();
    }

    getEffect() {
        return this.effect;
    }
}

module.exports = Reverb;