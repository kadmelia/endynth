// import * as Tone from 'tone';
// import {Keyboard} from './Keyboard';
// import {Reverb} from './Reverb';

// import {ipcRenderer} from 'electron';
const Tone = require('tone');
const Keyboard = require('./src/Keyboard');
const Reverb = require('./src/Reverb');
console.log(Tone);
const synth = new Tone.PolySynth().toDestination();
const effect = new Reverb();
const keyboard = new Keyboard(synth, effect);
